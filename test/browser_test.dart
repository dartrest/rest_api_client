@TestOn('browser')
@Timeout(Duration(seconds: 10))

import 'dart:convert';
import 'dart:html';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:rest_api_client/rest_api_client.dart';
import 'package:rest_api_client/src/multipart_request.dart';
import 'package:test/test.dart';

import 'helpers/image_data.dart';

void main() {
  Uri apiUri;
  ApiClient apiClient;

  setUpAll(() async {
    final channel = spawnHybridUri('helpers/http_server.dart', stayAlive: true);
    final String hostPort = await channel.stream.first;
    apiUri = Uri.http(hostPort, '/');
    apiClient = ApiClient(apiUri,
        onBeforeRequest: (request) => request.change(
            headers: Map.from(request.headers ?? {})
              ..addAll({'X-Requested-With': 'XMLHttpRequest'})),
        onAfterResponse: (response) =>
            response.change(headers: {'X-Added-By-Callback': 'value'}));
  });

  test('uploader', () async {
    final imageData = Uint8List.fromList(base64.decode(b64ImageData)).buffer;
    final file = File([imageData], 'test_image.jpg', {'type': 'image/jpeg'});
    int expectedSize = file.size;
    int onProgressCalledTimes = 0;
    bool onCompleteCalled = false;
    final response = await apiClient.send(UploadRequest(
        resourcePath: 'files',
        file: file,
        onProgress: (sentBytes, totalBytes) {
          ++onProgressCalledTimes;
        },
        onComplete: () {
          onCompleteCalled = true;
        }));
    int expectedOnProcessCalls = (expectedSize / (64 * 1024)).ceil();
    expect(response.statusCode, HttpStatus.ok);
    expect(response.body, {
      'type': 'image/jpeg',
      'name': 'test_image.jpg',
      'bytesCount': expectedSize
    });
    expect(onProgressCalledTimes, expectedOnProcessCalls);
    expect(onCompleteCalled, true);
  });

  test('multipart request browser test', () async {
    final fileContent = Uint8List.fromList(base64.decode(b64ImageData));
    final file = File([fileContent], 'test_image.jpg', {'type': 'image/jpeg'});
    int fileSize = file.size;

    MultipartRequest multipartRequest = MultipartRequest(
        resourcePath: 'form-multipart',
        headers: {'x-requested-with': 'XMLHttpRequest'});
    multipartRequest.files.add(http.MultipartFile.fromBytes('file', fileContent,
        filename: file.name, contentType: MediaType.parse(file.type)));

    final response = await apiClient.send(multipartRequest);
    expect(response.statusCode, HttpStatus.ok);
    int contentLength = int.parse(response.body['Content-Length']);
    String contentType = response.body['Content-Type'];
    expect(contentLength > fileSize, true);
    expect(contentType.contains('multipart/form-data'), true);
  });
}
