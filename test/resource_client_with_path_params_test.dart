@TestOn('vm')
import 'dart:convert';
import 'package:data_model/data_model.dart';
import 'package:meta/meta.dart';
import 'package:rest_api_client/rest_api_client.dart';
import 'package:test/test.dart';
import 'package:http/testing.dart';
import 'package:http/http.dart';

class TestResourceObject extends Model {
  Map<String, dynamic> _data;

  TestResourceObject.fromJson(Map<String, dynamic> json) : _data = json;

  @override
  Map<String, dynamic> get json => _data;
}

class TestResourceObjectId extends ObjectId {
  TestResourceObjectId(id) : super(id);
}

class ResourceWithPathParams extends ResourceClientWithPathParams {
  ResourceWithPathParams()
      : super(
            '/path/{param1}/path/{param2}/path',
            ApiClient(Uri.parse('http://test.server.com'),
                httpClient: MockClient((request) async {
              if (request.url.path != '/path/param1/path/param2/path') {
                throw Exception('Parameters do not match');
              }
              return Response(json.encode({}), 200);
            })));

  @override
  Model<ObjectId> createModel(Map<String, dynamic> json) =>
      TestResourceObject.fromJson({'id': 1});
}

void main() {
  group('path params', () {
    var resource = ResourceWithPathParams();
    final params = {
      'param1': 'param1',
      'param2': 'param2',
    };

    test('create', () async {
      await resource.create(TestResourceObject.fromJson({'id': 1}),
          params: params);
      expect(resource.lastResponse.statusCode, 200);
    });

    test('update', () async {
      await resource.update(TestResourceObject.fromJson({'id': 1}),
          params: params);
      expect(resource.lastResponse.statusCode, 200);
    });

    test('replace', () async {
      await resource.replace(TestResourceObject.fromJson({'id': 1}),
          params: params);
      expect(resource.lastResponse.statusCode, 200);
    });

    test('read', () async {
      await resource.read({'id': '1'}, params: params);
      expect(resource.lastResponse.statusCode, 200);
    });

    test('delete', () async {
      await resource.delete({'id': '1'}, params: params);
      expect(resource.lastResponse.statusCode, 200);
    });
  });
}
