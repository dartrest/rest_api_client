/// Поддержка работы с REST-ресурсами на стороне клиента.
///
/// ## Назначение
///
/// Библиотека предназначена для создания REST-клиентов (поддерживаются web- и
/// mobile-клиенты)
///
/// ## Структура
///
/// В основе библиотеки лежит класс `RestResource`, который для общения с
/// API-сервером использует `RestClient`. Объекты, с которыми работает
/// `RestResource` должны реализовывать интерфейс `JsonEncodable`.
///
/// `RestResource` изначально поддерживает стандартные методы (CRUD) работы с REST-ресурсами:
/// * `create` - создание объекта
/// * `read` - получение объекта/списка объектов
/// * `update` - частичное обновление данных объекта
/// * `replace` - полная замена данных объекта
/// * `delete` - удаление объекта
///
/// При наследовании ресурсы можно дополнять другими необходимыми методами.
library rest_api_client;

export 'package:universal_io/prefer_universal/io.dart'
    show ContentType, HttpStatus;

export 'src/api_client.dart';
export 'src/api_request.dart';
export 'src/api_response.dart';
export 'src/http_exception.dart';
export 'src/multipart_request.dart';
export 'src/resource_client.dart';
export 'src/resource_client_with_path_params.dart';
export 'src/upload_request.dart';
export 'src/uploader.dart';
