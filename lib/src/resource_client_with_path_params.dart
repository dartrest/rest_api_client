import 'package:rest_api_client/rest_api_client.dart';

import 'resource_client.dart';
import 'package:data_model/data_model.dart';

/// Resource client which can contain dynamic parameters in path
///
/// Parameters should be set in [resourcePath] as follows:
///
/// 'path/with/{param}/to/resource'
///
/// Values of parameters are set in [params] argument of overriden methods
abstract class ResourceClientWithPathParams<T extends Model>
    extends ResourceClient<T> {
  ResourceClientWithPathParams(String resourcePath, ApiClient apiClient)
      : super(resourcePath, apiClient);

  @override
  Future<T> create(T obj,
      {Map<String, String> headers = const {},
      Map<String, String> params = const {}}) {
    mkResourcePathBuilder(params);
    return super.create(obj, headers: headers);
  }

  @override
  Future<T> update(T obj,
      {Map<String, String> headers = const {},
      Map<String, String> params = const {}}) {
    mkResourcePathBuilder(params);
    return super.update(obj, headers: headers);
  }

  @override
  Future<T> replace(T obj,
      {Map<String, String> headers = const {},
      Map<String, String> params = const {}}) {
    mkResourcePathBuilder(params);
    return super.replace(obj, headers: headers);
  }

  @override
  Future read(obj,
      {Map<String, String> headers = const {},
      Map<String, String> params = const {}}) {
    mkResourcePathBuilder(params);
    return super.read(obj, headers: headers);
  }

  @override
  Future delete(obj,
      {Map<String, String> headers = const {},
      Map<String, String> params = const {}}) {
    mkResourcePathBuilder(params);
    return super.delete(obj, headers: headers);
  }

  @override
  String get resourcePath =>
      _resourcePath != null ? _resourcePath() : super.resourcePath;

  final _paramPattern = RegExp(r'\{(\w+)\}');

  String Function() _resourcePath;

  String _buildResourcePath(Map<String, String> params) => super
      .resourcePath
      .replaceAllMapped(_paramPattern, (m) => params[m.group(1)]);

  void mkResourcePathBuilder(Map<String, String> params) =>
      _resourcePath = () => _buildResourcePath(params);
}
