import 'dart:async';
import 'dart:html';

import 'package:http/http.dart' as http;

import 'uploader.dart';

Uploader createUploader() => BrowserUploader();

/// Uploader for use in browser code
///
/// Adopts JS File-interface to standard behavour with `onProgress`, `onComplete` and `onError` callbacks
///
/// This class is used internally and in most cases does not require to instantiate manually
class BrowserUploader implements Uploader {
  Future<http.Response> call(file, Uri uri,
      {Map<String, String> headers,
      OnProgress onProgress,
      OnComplete onComplete,
      Function onError}) {
    if (file is! File) throw ArgumentError('file must have type File');
    final streamedRequest = http.StreamedRequest('POST', uri);
    if (headers != null) streamedRequest.headers.addAll(headers);
    streamedRequest.headers.addAll({
      'content-type': file.type ?? 'application/octet-stream',
      'content-disposition':
          'attachment; filename=${Uri.encodeComponent(file.name)}'
    });
    final request = streamedRequest.send();
    final fileStream = _getFileStream(file);
    int bytesCount = 0;
    fileStream.listen((data) {
      streamedRequest.sink.add(data);
      bytesCount += data.length;
      if (onProgress != null) onProgress(bytesCount, file.size);
    }, onDone: () {
      streamedRequest.sink.close();
      if (onComplete != null) onComplete();
    }, onError: onError, cancelOnError: true);
    return request
        .then((responseStream) => http.Response.fromStream(responseStream));
  }

  Stream<List<int>> _getFileStream(File file) {
    final fileStreamController = StreamController<List<int>>();
    const chunkSize = 64 * 1024;
    int start = 0;
    int end = chunkSize;
    Future.doWhile(() {
      final completer = Completer<bool>();
      final fileReader = FileReader();
      fileReader.onLoad.listen((event) {
        fileStreamController.add(fileReader.result);
        if (end < file.size - 1) {
          start = end;
          end += chunkSize;
          completer.complete(true);
        } else {
          fileStreamController.close();
          completer.complete(false);
        }
      });
      fileReader.onError.listen((event) {
        fileStreamController.addError(fileReader.error);
        completer.complete(false);
      });
      var blob = file.slice(start, end);
      fileReader.readAsArrayBuffer(blob);
      return completer.future;
    });
    return fileStreamController.stream;
  }
}
