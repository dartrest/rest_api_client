import 'dart:async';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:mime/mime.dart';
import 'package:path/path.dart';

import 'uploader.dart';

Uploader createUploader() => VmUploader();

/// Uploader for use in VM code
///
/// Allows to use standard behavour with `onProgress`, `onComplete` and `onError` callbacks
/// on VM
///
/// This class is used internally and in most cases does not require to instantiate manually
class VmUploader implements Uploader {
  Future<http.Response> call(file, Uri uri,
      {Map<String, String> headers,
      OnProgress onProgress,
      OnComplete onComplete,
      Function onError}) async {
    final int fileLength = await file.length();

    if (file is! File) throw ArgumentError('file must have type File');
    final streamedRequest = http.StreamedRequest('POST', uri);
    final fileName = basename(file.path);
    final mimeType = lookupMimeType(fileName);
    if (headers != null) streamedRequest.headers.addAll(headers);
    streamedRequest.headers.addAll({
      'content-type': mimeType != null ? mimeType : 'application/octet-stream',
      'content-length': fileLength.toString(),
      'content-disposition':
          'attachment; filename=${Uri.encodeComponent(fileName)}'
    });

    final futureRequest = streamedRequest.send();

    int bytesCount = 0;
    file.openRead().listen(
        (data) {
          bytesCount += data.length;
          if (onProgress != null) onProgress(bytesCount, fileLength);
          streamedRequest.sink.add(data);
        },
        onError: onError,
        onDone: () {
          streamedRequest.sink.close();
          if (onComplete != null) onComplete();
        },
        cancelOnError: true);
    return futureRequest
        .then((responseStream) => http.Response.fromStream(responseStream));
  }
}
