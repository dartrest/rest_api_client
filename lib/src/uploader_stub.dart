import 'uploader.dart';

Uploader createUploader() => throw UnsupportedError(
    'Cannot create a uploader without dart:html or dart:io.');
